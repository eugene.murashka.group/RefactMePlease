﻿using Domain.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;

namespace Infrastructure.Data;
public static class InitialiserExtensions
{
    public static async Task InitialiseDatabaseAsync(this WebApplication app)
    {
        using var scope = app.Services.CreateScope();

        var initialiser = scope.ServiceProvider.GetRequiredService<ApplicationDbContextInitialiser>();

        await initialiser.InitialiseAsync();

        await initialiser.SeedAsync();
    }
}

public class ApplicationDbContextInitialiser
{
    private readonly ApplicationDbContext _context;
    public ApplicationDbContextInitialiser(ApplicationDbContext context)
    {
        _context = context;
    }

    internal async Task InitialiseAsync()
    {
        try
        {
            await _context.Database.MigrateAsync();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"{ex}\nAn error occurred while initialising the database.");
            throw;
        }
    }

    internal async Task SeedAsync()
    {
        try
        {
            await TrySeedAsync();
        }
        catch (Exception ex)
        {
            Debug.WriteLine($"{ex} --- An error occurred while seeding the database.");
            throw;
        }
    }

    private async Task TrySeedAsync()
    {
        if (!_context.TodoItems.Any())
        {
            _context.TodoItems.Add(new TodoItem
            {
                Note = "TestTodoItem",
            });

            await _context.SaveChangesAsync();
        }

        if (!_context.Notes.Any())
        {
            _context.Notes.Add(new Note
            {
                Text = "TestNote",
            });

            await _context.SaveChangesAsync();
        }
    }
}
