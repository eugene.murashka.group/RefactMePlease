﻿namespace Domain.Enums;
public enum TodoPriority
{
    Could,
    Should,
    Must,
}
