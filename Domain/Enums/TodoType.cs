﻿namespace Domain.Enums;
public enum TodoType
{
    Daily,
    Weekly,
    Monthly,
    Annually,
}
