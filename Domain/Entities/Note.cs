﻿namespace Domain.Entities;
public class Note
{
    public int Id { get; set; }
    public string Text { get; set; } = null!;
    public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
}
