﻿using Domain.Enums;

namespace Domain.Entities;
public class TodoItem
{
    public int Id { get; set; }
    public string? Note { get; set; }
    public bool Done { get; set; }
    public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
    public TodoType? Type { get; set; } = TodoType.Daily;
    public TodoPriority? Priority { get; set; } = TodoPriority.Could;
}
