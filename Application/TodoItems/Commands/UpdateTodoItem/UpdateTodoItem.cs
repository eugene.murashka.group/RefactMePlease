﻿using Application.Common.Interfaces;
using Domain.Enums;
using MediatR;

namespace Application.TodoItems.Commands.UpdateTodoItem;
public record UpdateTodoItemCommand : IRequest
{
    public int Id { get; init; }
    public string? Note { get; init; }
    public bool Done { get; init; } // TODO: Можно ли сделать NULLable
    public TodoType? Type { get; init; }
    public TodoPriority? Priority { get; init; }
}

public class UpdateTodoItemCommandHandler : IRequestHandler<UpdateTodoItemCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdateTodoItemCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task Handle(UpdateTodoItemCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.TodoItems
            .FindAsync(new object[] { request.Id }, cancellationToken);

        if (entity == null)
        {
            throw new ArgumentException(); // TODO: Guard.Against.NotFound(request.Id, entity);
        }

        entity.Note = request.Note != null ? request.Note : entity.Note;
        entity.Done = request.Done;
        entity.Type = request.Type != null ? request.Type : entity.Type;
        entity.Priority = request.Priority != null ? request.Priority : entity.Priority;

        await _context.SaveChangesAsync(cancellationToken);
    }
}
