﻿using Application.Common.Interfaces;
using Domain.Entities;
using Domain.Enums;
using MediatR;

namespace Application.TodoItems.Commands.CreateTodoItem;
public record CreateTodoItemCommand : IRequest<int>
{
    public string? Note { get; init; }
    public TodoType? Type { get; init; } = TodoType.Daily;
}

public class CreateTodoItemCommandHandler : IRequestHandler<CreateTodoItemCommand, int>
{
    private readonly IApplicationDbContext _context;

    public CreateTodoItemCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<int> Handle(CreateTodoItemCommand request, CancellationToken cancellationToken)
    {
        if (string.IsNullOrEmpty(request.Note))
        {
            throw new ArgumentNullException(nameof(request.Note));
        }

        if (request.Type == null)
        {
            throw new ArgumentNullException(nameof(request.Type));
        }

        // TODO: TodoType > max value = Exception

        var entity = new TodoItem
        {
            Note = request.Note,
            Type = request.Type,
        };

        _context.TodoItems.Add(entity);

        await _context.SaveChangesAsync(cancellationToken);

        return entity.Id;
    }
}
