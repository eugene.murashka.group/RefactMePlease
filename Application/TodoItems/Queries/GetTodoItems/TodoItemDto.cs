﻿using Domain.Entities;
using Domain.Enums;

namespace Application.TodoItems.Queries.GetTodoItems;
public class TodoItemDto
{
    public int Id { get; init; }

    public string? Note { get; init; }

    public bool Done { get; init; }

    public DateTime? CreatedDate { get; init; }

    public TodoType? Type { get; init; }

    public TodoPriority? Priority { get; init; }

    private class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<TodoItem, TodoItemDto>();
        }
    }
}
