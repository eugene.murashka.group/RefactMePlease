﻿using Application.Common.Interfaces;
using AutoMapper.QueryableExtensions;
using Domain.Entities;
using Domain.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Application.TodoItems.Queries.GetTodoItems;
public record GetTodoItemsQuery : IRequest<IEnumerable<TodoItemDto>>
{
    public int? Id { get; set; } = null;
    public bool? DoneStatus { get; set; } = null;
    public DateTime? Date { get; set; } = null;
    public TodoType? TodoType { get; set; } = null;
    public TodoPriority? TodoPriority { get; set; } = null;
}

public class GetTodoItemsHandler : IRequestHandler<GetTodoItemsQuery, IEnumerable<TodoItemDto>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetTodoItemsHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<IEnumerable<TodoItemDto>> Handle(GetTodoItemsQuery request, CancellationToken cancellationToken)
    {
        Expression<Func<TodoItem, bool>> filter = todo =>
            (request.Id != null
                ? todo.Id == request.Id
                : true) &&
            (request.DoneStatus != null
                ? todo.Done == request.DoneStatus
                : true) &&
            (request.Date != null
                ? todo.CreatedDate.Date == request.Date
                : true) &&
            (request.TodoType != null
                ? todo.Type == request.TodoType
                : true) &&
            (request.TodoPriority != null
                ? todo.Priority == request.TodoPriority
                : true);

        return await _context.TodoItems
            .Where(filter)
            .OrderBy(x => x.Id)
            .ProjectTo<TodoItemDto>(_mapper.ConfigurationProvider)
            .AsNoTracking()
            .ToArrayAsync();
    }
}
