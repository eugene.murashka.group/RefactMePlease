﻿using Domain.Entities;

namespace Application.Notes.Queries.GetNotes;
public class NoteDto
{
    public int Id { get; init; }
    public string Text { get; init; } = null!;
    public DateTime CreateDate { get; init; }

    private class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<Note, NoteDto>();
        }
    }
}
