﻿using Application.Common.Interfaces;
using AutoMapper.QueryableExtensions;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Application.Notes.Queries.GetNotes;
public record GetNotesQuery : IRequest<IEnumerable<NoteDto>>
{
    public int? Id { get; set; }
    public string? Text { get; set; }
    public DateTime? Date { get; set; }
}

public class GetNotesHandler : IRequestHandler<GetNotesQuery, IEnumerable<NoteDto>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetNotesHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<IEnumerable<NoteDto>> Handle(GetNotesQuery request, CancellationToken cancellationToken)
    {
        Expression<Func<Note, bool>> filter = note =>
            (request.Id != null
                ? note.Id == request.Id
                : true) &&
            (request.Text != null
                ? note.Text == request.Text
                : true) &&
            (request.Date != null
                ? note.CreatedDate.Date == request.Date
                : true);

        return await _context.Notes
            .Where(filter)
            .OrderBy(x => x.Id)
            .ProjectTo<NoteDto>(_mapper.ConfigurationProvider)
            .AsNoTracking()
            .ToArrayAsync();
    }
}
