﻿using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.Notes.Commands.CreateNote;
public record CreateNoteCommand : IRequest<int>
{
    public string Text { get; set; } = null!;
}

public class CreateNoteHandler : IRequestHandler<CreateNoteCommand, int>
{
    private readonly IApplicationDbContext _context;

    public CreateNoteHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
    }

    public async Task<int> Handle(CreateNoteCommand request, CancellationToken cancellationToken)
    {
        if (string.IsNullOrEmpty(request.Text))
        {
            throw new ArgumentNullException(nameof(request.Text));
        }

        var entity = new Note
        {
            Text = request.Text,
            CreatedDate = DateTime.UtcNow,
        };

        _context.Notes.Add(entity);

        await _context.SaveChangesAsync(cancellationToken);

        return entity.Id;
    }
}
