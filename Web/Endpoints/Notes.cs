﻿using Application.Notes.Commands.CreateNote;
using Application.Notes.Queries.GetNotes;

namespace Web.Endpoints;
public class Notes
{
    public static async Task<IEnumerable<NoteDto>> GetNotes(ISender sender, [AsParameters] GetNotesQuery query)
    {
        return await sender.Send(query);
    }

    public static async Task<int> CreateNote(ISender sender, CreateNoteCommand command)
    {
        return await sender.Send(command);
    }
}
