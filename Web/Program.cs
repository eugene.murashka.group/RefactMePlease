using Application.Notes.Commands.CreateNote;
using Application.Notes.Queries.GetNotes;
using Application.TodoItems.Commands.CreateTodoItem;
using Application.TodoItems.Commands.UpdateTodoItem;
using Application.TodoItems.Queries.GetTodoItems;
using Infrastructure.Data;
using Web.Endpoints;

var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddCors(options =>
{
    options.AddPolicy(name: MyAllowSpecificOrigins,
        policy =>
        {
            policy.WithOrigins("http://localhost:3000")
                .AllowAnyHeader()
                .AllowAnyMethod();
        });
});

builder.Services.AddInfrastructureServices(builder.Configuration);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddApplicationServices();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();

    await app.InitialiseDatabaseAsync();
}

app.UseHttpsRedirection();

app.UseCors(MyAllowSpecificOrigins);

app.MapGet("/", () => "Hello, World!")
    .WithName("Home")
    .WithOpenApi();

const string todoItemPoint = "TodoItem";
const string notePoint = "Note";

app.MapGet($"{todoItemPoint}", (ISender sender, [AsParameters] GetTodoItemsQuery query) =>
{
    return TodoItems.GetTodoItems(sender, query);
})
    .WithName("GetTodoItems")
    .WithOpenApi();


app.MapPost($"{todoItemPoint}", (ISender sender, CreateTodoItemCommand command) =>
{
    return TodoItems.CreateTodoItem(sender, command);
})
    .WithName("CreateTodoItem")
    .WithOpenApi();

app.MapPatch($"{todoItemPoint}", (ISender sender, int id, UpdateTodoItemCommand command) =>
{
    return TodoItems.UpdateTodoItem(sender, id, command);
})
    .WithName("UpdateTodoItem")
    .WithOpenApi();

app.MapDelete($"{todoItemPoint}", (ISender sender, int id) =>
{
    return TodoItems.DeleteTodoItem(sender, id);
})
    .WithName("DeleteTodoItem")
    .WithOpenApi();

app.MapGet($"{notePoint}", (ISender sender, [AsParameters] GetNotesQuery query) =>
{
    return sender.Send(query);
})
    .WithName("GetNotes")
    .WithOpenApi();

app.MapPost($"{notePoint}", (ISender sender, CreateNoteCommand command) =>
{
    return sender.Send(command);
})
    .WithName("CreateNote")
    .WithOpenApi();

app.Run();
